package com.example.demo.controller;


import com.example.demo.exception.TodoCollectionException;
import com.example.demo.model.TodoDto;
import com.example.demo.repository.TodoRepository;

import com.example.demo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@RestController
public class TodoController {
    @Autowired
    private TodoRepository todoRepository;
    @Autowired
    private TodoService todoService;

    @GetMapping("/todos")
    public ResponseEntity<?> getAllTodos() {

        List<TodoDto> todos = todoService.getAllTodos();
        return new ResponseEntity<>(todos,todos.size() >0  ? HttpStatus.OK : HttpStatus.NOT_FOUND);



    }

    @PostMapping("/addtodo")
    public ResponseEntity<?> saveBook(@RequestBody TodoDto todo) {
        try {

            todoService.CreatedTodo(todo);
            return new ResponseEntity<TodoDto>(todo, HttpStatus.OK);

        } catch (ConstraintViolationException e) {

            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }catch (TodoCollectionException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);

        }
    }
    @GetMapping("/todos/{id}")
    public ResponseEntity<?> getSingleTodo(@PathVariable("id") String id) {
try {
return  new ResponseEntity<>(todoService.getSingleTodo(id),HttpStatus.OK);
}catch (Exception e){
    return  new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);

}
    }
    @PutMapping("/todos/{id}")
    public ResponseEntity<?> updateTutorial(@PathVariable("id") String id, @RequestBody TodoDto todo) {
    try{
   todoService.UpdatedTodo(id,todo);
   return  new ResponseEntity<>("updated todo with id" +id,HttpStatus.OK);
    }catch (ConstraintViolationException  e){
        return new ResponseEntity<>(e.getMessage(),HttpStatus.UNPROCESSABLE_ENTITY);

    }catch (TodoCollectionException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }
    }

    @DeleteMapping("/todos/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") String id) {
        try {
            todoService.DeletedTodoById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (TodoCollectionException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/todos")
    public ResponseEntity<HttpStatus> deleteAllTutorials() {
        try {
            todoRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}