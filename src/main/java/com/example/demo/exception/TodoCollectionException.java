package com.example.demo.exception;



public class TodoCollectionException extends Exception{
    /**
     *
     */
    static final long serialVersionUID = 1L;

    public TodoCollectionException(String message) {
        super(message);
    }

    public static  String NotFoundException(String id){
        return  "todo with " +id+ " not found!!";
    }

    public static  String TododAlreadayExists(){
        return  "todo with given name alerady exists";
    }
}
