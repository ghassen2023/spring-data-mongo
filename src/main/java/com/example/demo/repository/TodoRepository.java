package com.example.demo.repository;


import com.example.demo.model.TodoDto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface TodoRepository extends MongoRepository<TodoDto,String> {


@Query("{ 'todo':?0}")
    Optional<TodoDto> findByTodo(String todo);
}
