package com.example.demo.service;


import com.example.demo.exception.TodoCollectionException;
import com.example.demo.model.TodoDto;

import javax.validation.ConstraintViolationException;
import java.util.List;

public interface TodoService {

    public void CreatedTodo(TodoDto todoDto) throws ConstraintViolationException, TodoCollectionException;

    public List<TodoDto> getAllTodos();

    public TodoDto getSingleTodo(String id) throws TodoCollectionException;

    public void UpdatedTodo(String id, TodoDto todoDto) throws TodoCollectionException;

    public void DeletedTodoById(String id) throws TodoCollectionException;

}
