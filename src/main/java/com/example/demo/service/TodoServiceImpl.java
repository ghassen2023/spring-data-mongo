package com.example.demo.service;


import com.example.demo.exception.TodoCollectionException;
import com.example.demo.model.TodoDto;
import com.example.demo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoRepository todoRepository;


    @Override
    public void CreatedTodo(TodoDto todoDto) throws ConstraintViolationException, TodoCollectionException {
        Optional<TodoDto> todoDtoOptional = todoRepository.findByTodo(todoDto.getTodo());
        if (todoDtoOptional.isPresent()) {
            throw new TodoCollectionException(TodoCollectionException.TododAlreadayExists());
        } else {
            todoDto.setCreatedAt(new Date(System.currentTimeMillis()));
            todoRepository.save(todoDto);
        }


    }

    @Override
    public List<TodoDto> getAllTodos() {
        List<TodoDto> todos = todoRepository.findAll();
        if (todos.size() > 0) {
            return todos;
        } else {
            return new ArrayList<TodoDto>();
        }

    }

    @Override
    public TodoDto getSingleTodo(String id) throws TodoCollectionException {
        Optional<TodoDto> todoDtoOptional = todoRepository.findById(id);
        if (!todoDtoOptional.isPresent()) {
            throw new TodoCollectionException(TodoCollectionException.NotFoundException(id));
        } else {
            return todoDtoOptional.get();
        }
    }

    @Override
    public void UpdatedTodo(String id, TodoDto todoDto) throws TodoCollectionException {
        Optional<TodoDto> todoDtoOptional = todoRepository.findById(id);
        if (!todoDtoOptional.isPresent()) {
            TodoDto todoUpdated = todoDtoOptional.get();
            todoUpdated.setCompleted(todoDto.getCompleted());
            todoUpdated.setDescription(todoDto.getDescription());
            todoUpdated.setTodo(todoDto.getTodo());
            todoUpdated.setUpdatedAt(new Date(System.currentTimeMillis()));
            todoRepository.save(todoUpdated);
        } else {
            throw new TodoCollectionException(TodoCollectionException.NotFoundException(id));

        }
    }

    @Override
    public void DeletedTodoById(String id) throws TodoCollectionException {
        Optional<TodoDto> todoDtoOptional = todoRepository.findById(id);
        if (!todoDtoOptional.isPresent()) {
            throw new TodoCollectionException(TodoCollectionException.NotFoundException(id));

        } else {
            todoRepository.deleteById(id);
        }
    }
}